

require 'rubygems'
require 'json'
require 'nokogiri'
require 'net/http'
class Location < ApplicationRecord
   self.ignored_columns = %w(class)

VALID_URL_REGEX = /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?\Z/i
validates :url, presence: true, format: { with: VALID_URL_REGEX }
before_save :getdata
geocoded_by :ip
after_validation :geocode


private
def getdata

uri = ARGV[0]
response = Net::HTTP.get(URI(url))
json = JSON.parse(response)
 
doc = Nokogiri::HTML(response)
doc.at_css('title').text
doc.to_html
end


end



 



