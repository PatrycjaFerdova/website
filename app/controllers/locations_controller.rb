

class LocationsController < ApplicationController


	  def index
      if params[:search].present?
      @locations = Location.search(params[:search])
    else
        @locations = Location.all
   end
    end

    def show
    @location = Location.find(params[:id])
    render :show => @location 
    end

  def new
     @location = Location.new
  end



  def create
 
     @location = Location.new(allowed_params)
     if @location.save
      redirect_to @location, :notice => "Successfully created location"
   else
      render :index
    end
  end


private

def search_params
  params.require(:search).permit(:id)
end




def allowed_params
    params.require(:location).permit(:id, :url, :ip, :email, :title, :country, :city, :latitude, :longitude)
  end
end
